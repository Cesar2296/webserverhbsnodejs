const express = require('express')
const app = express()
const port=process.env.PORT||8080
const hbs = require('hbs')

app.use(express.static(__dirname+'/public'))
hbs.registerPartials(__dirname+'/views/partials')


app.set('view engine', 'hbs')

app.get('/', (req, res)=> {
   res.render('home',{
       name:'Eduardo Celedonio'
   })
})

app.get('/about', (req, res)=> {
    res.render('about')
 })

app.get('/name', (req, res)=> {
    let salida={
        nombre:'Cesar'
    }  
  res.send(salida)
})

app.get('/data',(req,res)=>{
    res.send('Hola data')
})
 
app.listen(port,()=>{
    console.log(`Escuchando en puerto ${port}`)
})