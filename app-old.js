const http=require('http');
http.createServer((req,res)=>{
    let salida={
        nombre:'Cesar'
    }
    res.writeHead(200,{'Content-Type':'application/json'});
    res.write(JSON.stringify(salida));
    res.end();
}).listen(8080);
console.log("Escuchando en puerto 8080");